﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    public float MoveSpeed = 10;
    public float CameraSpeed = 10;

    Rigidbody rb;
    CharacterController controller;
    Vector2 move, look;

    float y, x;

    void OnMove(InputValue value)
    {
        move = value.Get<Vector2>();
    }

    void OnLook(InputValue value)
    {
        look = value.Get<Vector2>();
    }

    void OnFire()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        controller = GetComponent<CharacterController>();
    }

    private void Update()
    {
        y += look.y * CameraSpeed * Time.deltaTime;
        y = Mathf.Clamp(y, -90, 90);
        x += look.x * CameraSpeed * Time.deltaTime;

        Vector3 euler = new Vector3(y, x, transform.eulerAngles.z);
        transform.eulerAngles = euler;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 movement = transform.right * move.x + transform.forward * move.y;
        controller.SimpleMove(movement * MoveSpeed);
    }
}

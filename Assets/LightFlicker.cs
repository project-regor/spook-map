﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour
{
    Light Light;
    float BaseIntensity;

    private void Start()
    {
        Light = GetComponent<Light>();
        BaseIntensity = Light.intensity;
    }

    // Update is called once per frame
    void Update()
    {
        if (Random.value < 0.05)
            Light.intensity = BaseIntensity - Light.intensity;
    }
}
